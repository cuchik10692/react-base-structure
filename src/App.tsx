import * as React from 'react';
import './App.css';

import { StarshipsPage } from '@components/pages'

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <StarshipsPage />
      </div>
    );
  }
}

export default App;
