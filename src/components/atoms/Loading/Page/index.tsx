import * as React from 'react'

import { withStyles, CircularProgress } from '@material-ui/core'

const _PageLoading = ({classes}) => (
  <div className={classes.loadingWrapper}>
    <div className={classes.loadingInner}>
      <CircularProgress className={classes.loading} />
    </div>
  </div>
);

export const PageLoading = withStyles({
  loadingWrapper: {
    position: 'fixed',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    display: 'table',
    width: '100%',
    height: '100%',
    '&:before': {
      position: 'absolute',
      display: 'block',
      width: '100%',
      height: '100%',
      top: 0,
      left: 0,
      background: 'rgba(0, 0, 0, 0.5)',
      content: '""'
    }
  },
  loadingInner: {
    display: 'table-cell',
    verticalAlign: 'middle',
  }
})(_PageLoading)