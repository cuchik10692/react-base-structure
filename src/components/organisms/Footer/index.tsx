import * as React from 'react'

import { withStyles, Typography } from '@material-ui/core'

const styles = theme => ({
  footer: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing.unit * 8,
    padding: `${theme.spacing.unit * 6}px 0`,
  },
})

const _Footer = ({classes}) => (
  <footer className={classes.footer}>
    <Typography variant="h6" align="center" gutterBottom>
      Luan Nguyen
    </Typography>
    <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
      Copyright 2019
    </Typography>
  </footer>
);

export const Footer = withStyles(styles)(_Footer)