import * as React from 'react'

import { withStyles, Toolbar, Typography, IconButton } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'

const styles = theme => ({
  toolbarMain: {
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
  },
});

const sections = [];

const _Header = ({classes}) => (
  <header>
    <Toolbar className={classes.toolbarMain}>
      <Typography
        component="h2"
        variant="h5"
        color="inherit"
        align="center"
        noWrap
        className={classes.toolbarTitle}
      >
        Coding Challenge
      </Typography>
      <IconButton>
        <SearchIcon />
      </IconButton>
    </Toolbar>
    <Toolbar variant="dense" className={classes.toolbarSecondary}>
      {sections.map(section => (
        <Typography color="inherit" noWrap key={section}>
          {section}
        </Typography>
      ))}
    </Toolbar>
  </header>
);

export const Header = withStyles(styles)(_Header)