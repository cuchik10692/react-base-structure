import { IStarshipsState } from '@store/starships';

export interface IProps {
  starshipsStore?: IStarshipsState
  fetchStarships?: () => void;
  classes?: any
}

export interface IState {
  distance: number;
}
