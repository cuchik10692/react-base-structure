import * as React from 'react';
import * as _ from 'lodash';

import { IProps, IState } from "./PropsState";
import { connect } from 'react-redux';
import { IRootReducer } from '@store/index'
import { starshipsActions } from '@store/starships'
import { IStarshipModel } from '@model/starships';
import { PageDefaultTemplate } from '@components/templates'
import { withStyles, Grid, Button, Table, TableHead, TableRow, TableBody, TableCell, TextField } from '@material-ui/core';
import { ActivityStatus } from '@common/enum';
import { PageLoading } from '@components/atoms';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
});

const DAY_HOUR = 24;
const MONTH_DAYS = 30;
const YEAR = 365;
const WEEK_DAYS = 7;

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

class _StarshipsPage extends React.Component<IProps, IState> {
  get consumableValue() {
    return {
      years: DAY_HOUR * YEAR,
      year: DAY_HOUR * YEAR,
      months: DAY_HOUR * MONTH_DAYS,
      month: DAY_HOUR * MONTH_DAYS,
      week: DAY_HOUR * WEEK_DAYS,
      weeks: DAY_HOUR * WEEK_DAYS,
      days: DAY_HOUR,
      day: DAY_HOUR,
    }
  }

  constructor(props: Readonly<IProps>) {
    super(props);

    this.state = {
      distance: 1000000
    };
  }

  getResuppliesNeeded = (starship: IStarshipModel) => {
    const { distance } = this.state;
    const split = starship.consumables!.split(' ');
    const numConsumables = Number(split[0]) * this.consumableValue[split[1]];
    const totalDistance = numConsumables * parseFloat(starship.MGLT);
    const result = distance / totalDistance;

    if (isNaN(result)) {
      return '-';
    }

    return result.toFixed(2);
  };

  componentDidMount = () => {
    this.props.fetchStarships!();
  };

  handleSubmit = (e: any) => {
    e.preventDefault();

    const { distance } = this.state;

    if (!distance) {
      alert('Distance is invalid.');
      return;
    }
  };

  handleOnChange = (e: any) => {
    this.setState({
      distance: e.target.value
    });
  };

  componentWillReceiveProps(nextProps: any) {
    console.log('===', nextProps);
  }

  renderRows = (classes) => {
    const { starshipsStore } = this.props;
    const starshipsRows = starshipsStore!.starships || [];
    const activityStatus = starshipsStore!.activityStatus;

    if (activityStatus === ActivityStatus.Loaded && !_.isEmpty(starshipsRows)) {
      return starshipsRows.map((starship: IStarshipModel, i: number) => {
        return (
          <TableRow className={classes.row} key={i}>
            <CustomTableCell>{i + 1}</CustomTableCell>
            <CustomTableCell>{starship.name}</CustomTableCell>
            <CustomTableCell align="right">{starship.MGLT}</CustomTableCell>
            <CustomTableCell align="right">{starship.consumables}</CustomTableCell>
            <CustomTableCell align="right">{this.getResuppliesNeeded(starship)}</CustomTableCell>
          </TableRow>
        );
      });
    }

    return null;
  };

  render() {
    const { classes, starshipsStore } = this.props;
    const activityLoading = starshipsStore!.activityStatus;
    console.log(activityLoading);
    return (
      <div>
        <PageDefaultTemplate>
          <Grid container>
            <Grid item md={10}>
              <TextField type="text" value={this.state.distance} onChange={this.handleOnChange} className="formControl" fullWidth margin="normal" />
            </Grid>
            <Grid item md={2} container>
              <Button variant="contained" color="primary" className={classes.button} size="large" onClick={this.handleSubmit} fullWidth>
                Calculate
              </Button>
            </Grid>
            {activityLoading === ActivityStatus.Loading && <PageLoading />}
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <CustomTableCell>#</CustomTableCell>
                  <CustomTableCell>Name</CustomTableCell>
                  <CustomTableCell align="right">Consumable</CustomTableCell>
                  <CustomTableCell align="right">MGLT</CustomTableCell>
                  <CustomTableCell align="right">Resupplies Need </CustomTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                { this.renderRows(classes) }
              </TableBody>
            </Table>
          </Grid>
        </PageDefaultTemplate>
      </div>
    );
  }
}

const mapStateToProps: any = (rootReducer: IRootReducer) => {
  return { starshipsStore: rootReducer.starshipsReducer };
}

export const StarshipsPage = connect(mapStateToProps, {
  fetchStarships: starshipsActions.fetchStarships
})(withStyles(styles)(_StarshipsPage));