import * as React from 'react'
import * as PropTypes from 'prop-types'

import { withStyles, CssBaseline } from '@material-ui/core'
import { Header, Footer } from '@components/organisms'

const styles = theme => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
});


// const social = ['GitHub', 'Twitter', 'Facebook'];

const _PageDefaultTemplate = ({
  classes, children
}) => {
  return (
    <React.Fragment>
      <CssBaseline />
      <div className={classes.layout}>
        <Header />
        <main> {children} </main>
        <Footer />
      </div>
    </React.Fragment>
  )
}

_PageDefaultTemplate.propTypes = {
  children: PropTypes.any.isRequired,
  classes: PropTypes.object.isRequired,
}
export const PageDefaultTemplate = withStyles(styles)(_PageDefaultTemplate)