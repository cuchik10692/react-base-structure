export interface IBaseParameter {
}
export class BaseParameter implements IBaseParameter {
  /**
   *
   */
  constructor(opt: IBaseParameter) {
    Object.keys(opt).forEach(key => this[key] = opt[key])

  }
}