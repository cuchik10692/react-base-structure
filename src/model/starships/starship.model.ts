import { IBaseModel, BaseModel } from "@model/index";

export interface IStarshipModel extends IBaseModel {
  name: string
  consumables: string
  MGLT: string

}
export class StarshipModel extends BaseModel implements IStarshipModel {
  /**
   *
   */
  name: string = ''
  consumables: string = ''
  MGLT: string = ''
  constructor(opt: IStarshipModel) {
    super(opt);
  }
}