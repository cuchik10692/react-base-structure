import { BaseParameter, IBaseParameter } from "@model/index";

export interface IStarshipsParameter extends IBaseParameter {
  page?: number
}
export class StarshipsParameter extends BaseParameter implements IStarshipsParameter {
  /**
   *
   */
  page?: number = 1

  constructor(opt: IStarshipsParameter = {}) {
    super(opt);
  }
}