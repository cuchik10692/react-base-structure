import { BaseService } from "@service/base.service";
import { SwapiURL } from '@common/urls/swapi';
import { IStarshipsParameter, IStarshipModel } from '@model/starships';

export interface IStarshipsService {
  fetchStarships(param: IStarshipsParameter): Promise<any>
}
export class StarshipsService extends BaseService implements IStarshipsService {
  /**
   *
   */
  result: IStarshipModel[];
  constructor(path?: string) {
    super(path!);
    this.result = [];
  }

  /**
   * Fetch list starships
   * @param param IStarshipsParameter
   */
  public fetchStarships(param: IStarshipsParameter): Promise<any> {
    const page = param ? param.page : 1;
    return super.select(SwapiURL.Starship, { format: 'json', page })
      .then((response: any) => {
        if (response.next) {
          this.result.push(...response.results);
          return this.fetchStarships({page: page ? page + 1 : 1});
        }
        else {
          this.result.push(...response.results);
        }
  
        return this.result;
      })
      .catch((error: any) => {
        console.log(error);
        return Promise.reject(error)
      })
  }

}