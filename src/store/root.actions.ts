import { ActivityStatus } from "@common/enum";
import { ITypedAction } from "@model/generic";

export interface IRootAction {
  loadingActivity?(activity: ActivityStatus, type: string): ITypedAction<ActivityStatus>
}
export class RootAction implements IRootAction {
  loadingActivity(activity: ActivityStatus, type: string): ITypedAction<ActivityStatus> {
    return { type, payload: activity }
  }
}
