import { combineReducers, Reducer } from 'redux'
import { ITypedAction } from '@model/generic';
import { starshipsReducer } from './starships/starships.reducers';
import { IStarshipsState } from './starships/starships.state';

export interface IRootReducer {
  starshipsReducer: Reducer<IStarshipsState, ITypedAction>
}

const reducers = combineReducers(<IRootReducer>{
  starshipsReducer
})

export default reducers
