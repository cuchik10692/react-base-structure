import { all, fork } from "redux-saga/effects";
import { watchFetchStarships } from './starships/starships.sagas'

export function* rootSaga() {
  yield all([
    fork(watchFetchStarships)
  ])
}