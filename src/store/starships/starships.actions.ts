import { IRootAction, RootAction } from "@store/index";
import { ITypedAction } from "@model/generic";
import { StarshipsTypes } from "./starships.types";
import { IStarshipsParameter } from "@model/starships";

export interface IStarshipsActions extends IRootAction {
  fetchStarships(param: IStarshipsParameter, resolve: any, reject: any): ITypedAction<IStarshipsParameter>
  receivedStarships(payload: any): ITypedAction
}
export class StarshipsActions extends RootAction implements IStarshipsActions {

  fetchStarships(param: IStarshipsParameter, resolve: any, reject: any): ITypedAction<IStarshipsParameter> {
    return { type: StarshipsTypes.FETCH_STARSHIPS, payload: param, meta: { resolve, reject } }
  }

  receivedStarships(payload: any): ITypedAction {
    return { type: StarshipsTypes.FETCH_STARSHIPS_SUCCESS, payload }
  }
}

export const starshipsActions = new StarshipsActions