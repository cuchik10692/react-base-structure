import { ActivityStatus } from "@common/enum";
import { IStarshipsState } from './starships.state';
import { IStarshipModel } from '@model/starships';

export interface IStarshipsMutations {
  receiveStarships(state: IStarshipsState, starships: IStarshipModel[]): IStarshipsState
}

export class StarshipsMutations implements IStarshipsMutations {

  receiveStarships(state: IStarshipsState, starships: IStarshipModel[]): IStarshipsState {
    return Object.assign({}, state, {
      activityStatus: ActivityStatus.Loaded,
      error: null,
      starships,
    })
  }
}