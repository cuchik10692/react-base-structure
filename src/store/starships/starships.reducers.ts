import { ITypedAction } from "@model/generic";
import { StarshipsState } from './starships.state'
import { IStarshipsMutations, StarshipsMutations } from './starships.mutations';
import { StarshipsTypes } from './starships.types';
import { IStarshipsState } from './starships.state'
import { ActivityStatus } from "@common/enum";

const initialStarshipsState = new StarshipsState();

const mutation: IStarshipsMutations = new StarshipsMutations


export const starshipsReducer =
  (state: IStarshipsState = initialStarshipsState, action: ITypedAction): IStarshipsState => {

    switch (action.type) {
      case StarshipsTypes.FETCH_STARSHIPS_LOADING:
        return { ...state, starships: [],
          activityStatus: ActivityStatus.Loading,
          error: null }
      case StarshipsTypes.FETCH_STARSHIPS_SUCCESS:
        return mutation.receiveStarships(state, action.payload)
      default:
        return state;
    }

  }