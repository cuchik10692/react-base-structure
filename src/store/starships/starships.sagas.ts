import { takeLatest, put } from "redux-saga/effects";
import { ITypedAction } from "@model/generic";
import { ActivityStatus } from "@common/enum";
import { IStarshipsService, StarshipsService } from '@service/starships.service';
import { StarshipsTypes } from './starships.types';
import { starshipsActions } from './starships.actions'

const service: IStarshipsService = new StarshipsService

export function* fetchStarships(action: ITypedAction) {
  try {
    yield put(starshipsActions.loadingActivity(ActivityStatus.Loading,
      StarshipsTypes.FETCH_STARSHIPS_LOADING))

    const result = yield service.fetchStarships(action.payload);

    yield put(starshipsActions.receivedStarships(result))
  }
  catch (ex) {
    console.log('error');
  }
}

export function* watchFetchStarships() {
  yield takeLatest(StarshipsTypes.FETCH_STARSHIPS, fetchStarships)
}
