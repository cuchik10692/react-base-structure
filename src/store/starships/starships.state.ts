import { IRootState } from "@store/index";
import { ActivityStatus } from "@common/enum";

export interface IStarshipsState extends IRootState {
  activityStatus?: ActivityStatus
  starships: []
  error: null
}
export class StarshipsState implements IStarshipsState {
  /**
   *
   */
  activityStatus?: ActivityStatus
  starships: []
  error: null

  constructor() {
    this.starships = [];
    this.activityStatus = ActivityStatus.NoActivity;
    this.error = null;
  }

}