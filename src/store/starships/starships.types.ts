export const StarshipsTypes = {
  FETCH_STARSHIPS: 'FETCH_STARSHIPS',
  FETCH_STARSHIPS_LOADING: 'FETCH_STARSHIPS_LOADING',
  FETCH_STARSHIPS_SUCCESS: 'FETCH_STARSHIPS_SUCCESS',
  FETCH_STARSHIPS_FAILURE: 'FETCH_STARSHIPS_FAILURE',
}